/**
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

#import "AppDelegate.h"

#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.window.frame), CGRectGetHeight(self.window.frame))];
  
  RCTRootView *redView = [self createViewWithLaunchOptions:launchOptions
                                              codeLocation:@"red.ios"];
  redView.frame = CGRectMake(0, 0, CGRectGetWidth(view.frame) / 2, CGRectGetHeight(view.frame));
  [view addSubview:redView];
  
  RCTRootView *blueView = [self createViewWithLaunchOptions:launchOptions
                                               codeLocation:@"blue.ios"];
  blueView.frame = CGRectMake(CGRectGetWidth(view.frame) / 2, 0, CGRectGetWidth(view.frame) / 2, CGRectGetHeight(view.frame));
  [view addSubview:blueView];
  
  UIViewController *rootViewController = [UIViewController new];
  rootViewController.view = view;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (RCTRootView *)createViewWithLaunchOptions:(NSDictionary *)launchOptions
                                codeLocation:(NSString *)location {
  NSURL *jsCodeLocation = [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:location
                                                                         fallbackResource:nil];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBundleURL:jsCodeLocation
                                                      moduleName:@"HaulExample"
                                               initialProperties:nil
                                                   launchOptions:launchOptions];
  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];
  
  return rootView;
}

@end
