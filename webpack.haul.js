module.exports = ({ platform }, { module, resolve }) => ({
  entry: {
    red: `./src/RedApp/index.${platform}.ts`,
    blue: `./src/BlueApp/index.${platform}.ts`
  },
  output: {
    path: "/static/dist",
    filename: "[name].ios.bundle"
  },
  module: {
    ...module,
    rules: [
      {
        test: /\.tsx?$/,
        exclude: '/node_modules/',
        use: [
          {
            loader: 'babel-loader',
          },
          {
            loader: 'ts-loader'
          },
        ],
      },
      ...module.rules
    ]
  },
  resolve: {
    ...resolve,
    extensions: [
      '.ts',
      '.tsx',
      `.${platform}.ts`,
      '.native.ts',
      `.${platform}.tsx`,
      '.native.tsx',
      ...resolve.extensions],
  },
});