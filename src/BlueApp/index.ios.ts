import 'haul/hot/patch';
import {
  makeHot,
  tryUpdateSelf,
  callOnce,
  clearCacheFor,
  redraw
} from 'haul/hot';
import { AppRegistry } from 'react-native';
import App from './App';

AppRegistry.registerComponent('HaulExample', makeHot(() => App))