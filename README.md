# README #

Демонстрационный проект использования Haul, как замены RN Packager

### Установка? ###

* В корневой директории выполнить команду: 
```bash 
yarn
```
* Перейти к директории ios
* Выполнить команды: 
```bash 
bundle
bundle exec pod install
```
* В корневой директории выполнить команду: 
```bash 
yarn haul start --platform ios
```